import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PacienteComponent } from './_pages/paciente/paciente.component';
import { HttpClientModule } from '@angular/common/http';
import { PacienteEdicionComponent } from './_pages/paciente/paciente-edicion/paciente-edicion.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MedicoComponent } from './_pages/medico/medico.component';
import { MedicoModalComponent } from './_pages/medico/medico-modal/medico-modal.component';
import { ExamenComponent } from './_pages/examen/examen.component';
import { EspecialidadComponent } from './_pages/especialidad/especialidad.component';
import { EspecialidadEdicionComponent } from './_pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { ExamenEdicionComponent } from './_pages/examen/examen-edicion/examen-edicion.component';
import { ConsultaComponent } from './_pages/consulta/consulta.component';

@NgModule({
  declarations: [
    AppComponent,
    PacienteComponent,
    PacienteEdicionComponent,
    MedicoComponent,
    MedicoModalComponent,
    ExamenComponent,
    EspecialidadComponent,
    EspecialidadEdicionComponent,
    ExamenEdicionComponent,
    ConsultaComponent
  ],
  entryComponents: [MedicoModalComponent], //aqui tenemos que indicar cuales seran los componentes modales
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule, //aqui importamos el modulo de material
    HttpClientModule, //modulo que nos permite hacer peticiones http
    ReactiveFormsModule, //modulo que nos permite la utilizacion de forms en angular
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
