import { MatPaginatorImpl } from './mat-paginator';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DATE_LOCALE,
         MatButtonModule, 
         MatSidenavModule,
         MatToolbarModule,
         MatIconModule,
         MatMenuModule,
         MatDividerModule,
         MatFormFieldModule,
         MatInputModule,
         MatTableModule,
         MatPaginatorModule,
         MatCardModule,
         MatSnackBarModule,
         MatDialogModule,
         MatSortModule,
         MatPaginatorIntl,
         MatSelectModule,
         MatDatepickerModule,
         MatNativeDateModule} from '@angular/material';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSortModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: MatPaginatorImpl },
    { provide : MAT_DATE_LOCALE, useValue:'es-ES'}
  ]
})
export class MaterialModule { }
