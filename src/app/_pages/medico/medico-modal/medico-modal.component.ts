import { MSJ_MOD, MSJ_INS } from './../../../shared/var.constants';
import { MedicoService } from './../../../_service/medico.service';
import { Medico } from './../../../_model/medico';
import { Component, OnInit, inject, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-medico-modal',
  templateUrl: './medico-modal.component.html',
  styleUrls: ['./medico-modal.component.css']
})
export class MedicoModalComponent implements OnInit {

  registro : Medico;
  //Aqui inyectamos un objeto que tiene la referencia de los datos enviados desde el componente invocador
  //@Inject(MAT_DIALOG_DATA) public obj : MiClase, es la manera de inyectar el objeto enviado desde el componente invocador
  //Inyectamos el service de medico
  constructor(private modalRef : MatDialogRef<MedicoModalComponent>, @Inject(MAT_DIALOG_DATA) public obj : Medico, private service : MedicoService) { }

  ngOnInit() {
    //this.registro = this.obj;
    this.registro = new Medico();
    this.registro.idMedico = this.obj.idMedico;
    this.registro.nombres = this.obj.nombres;
    this.registro.apellidos = this.obj.apellidos;
    this.registro.registro = this.obj.registro;
  }

  cancelar(){
    this.modalRef.close();
  }

  operar(){
    if (this.registro != null && this.registro.idMedico > 0){
      this.service.modificar(this.registro).subscribe(obj => {
        this.service.listar().subscribe(datos => {
          this.service.registroCambio.next(datos);
          this.service.mensajeCambio.next(MSJ_MOD);
        });
      });
    }
    else{
      this.service.modificar(this.registro).subscribe(obj => {
        this.service.listar().subscribe(datos => {
          this.service.registroCambio.next(datos);
          this.service.mensajeCambio.next(MSJ_INS);
        });
      });
    }
    this.modalRef.close();
  }
}
