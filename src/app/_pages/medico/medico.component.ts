import { MSJ_DEL } from './../../shared/var.constants';
import { MedicoModalComponent } from './medico-modal/medico-modal.component';
import { MedicoService } from './../../_service/medico.service';
import { Medico } from './../../_model/medico';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  obj : Medico;

  listaDataSource: MatTableDataSource<Medico>; //MatTableDataSource sera un datasource de medicos
  columnasMostradas = ['idMedico','nombres','apellidos','documento','acciones'];
  @ViewChild(MatPaginator) paginator : MatPaginator; //el @viewchild enlaza el componente mat-paginator del html al componente,para usarlo aqui
  @ViewChild(MatSort) sort :MatSort; //el @viewchild enlaza el componente mat-sort del html al componente,para usarlo aqui

  //inyectamos el service de medicos en el constructor
  //inyectamos un objeto MatSnackBar para mostrar alertas
  //inyectamos un objeto MatDialog para poder dar la funcional de ventana modal
  constructor(private medicoService : MedicoService, private alerta :  MatSnackBar, private modal : MatDialog) { }

  ngOnInit() {
    this.medicoService.registroCambio.subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;      
    });

    //Mostramos la alerta
    this.medicoService.mensajeCambio.subscribe(mensaje => {
      this.alerta.open(mensaje,'Aviso',{duration:3000});
    });

    this.medicoService.listar().subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.listaDataSource.filter = filterValue;
  }

  eliminar(id : number){
    this.medicoService.eliminar(id).subscribe(data => {
      this.medicoService.listar().subscribe(data => {
        this.medicoService.registroCambio.next(data);
        this.medicoService.mensajeCambio.next(MSJ_DEL);
      });
    });
  }

  openModal(obj : Medico){
    let medico = obj != null ? obj : new Medico();
    this.modal.open(MedicoModalComponent, {
      width: '400px',
      disableClose : true,
      data : medico
    });
  }

}
