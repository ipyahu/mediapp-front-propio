import { MedicoService } from './../../_service/medico.service';
import { Especialidad } from './../../_model/especialidad';
import { PacienteService } from './../../_service/paciente.service';
import { Paciente } from './../../_model/paciente';
import { Component, OnInit } from '@angular/core';
import { EspecialidadService } from '../../_service/especialidad.service';
import { Medico } from '../../_model/medico';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  pacientes : Paciente[] = [];
  especialidades : Especialidad[] = [];
  medicos : Medico[] = [];

  fechaSeleccionada : Date = new Date();
  maxFecha : Date = new Date();

  constructor(private pacienteService : PacienteService,
              private especialidadService : EspecialidadService,
              private medicoService : MedicoService) 
  { 

  }

  ngOnInit() {
    this.listarPacientes();
    this.listarEspecialidades();
    this.listarMedicos();
  }

  listarPacientes(){
    this.pacienteService.listar().subscribe(datos => {
      this.pacientes = datos;
    });
  }

  listarEspecialidades(){
    this.especialidadService.listar().subscribe(datos => {
      this.especialidades = datos;
    });
  }

  listarMedicos(){
    this.medicoService.listar().subscribe(datos => {
      this.medicos = datos;
    });
  }
}
