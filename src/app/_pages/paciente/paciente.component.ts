import { MSJ_DEL } from './../../shared/var.constants';
import { PacienteService } from './../../_service/paciente.service';
import { Paciente } from './../../_model/paciente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  listaDataSource: MatTableDataSource<Paciente>; //MatTableDataSource sera un datasource de pacientes
  columnasMostradas = ['idPaciente','nombres','apellidos','documento','direccion','acciones'];
  @ViewChild(MatPaginator) paginator : MatPaginator; //el @viewchild enlaza el componente mat-paginator del html al componente,para usarlo aqui
  @ViewChild(MatSort) sort :MatSort; //el @viewchild enlaza el componente mat-sort del html al componente,para usarlo aqui
  cantRegistros : number;


  //inyectamos el service de pacientes en el constructor
  //inyectamos un objeto MatSnackBar para mostrar alertas
  constructor(private service : PacienteService, private alerta :  MatSnackBar) { }

  //al instanciarse la clase, ya hacemos la peticion get
  ngOnInit() {
    //pacienteCambio es la variable reactiva que tiene la lista de pacientes segun las ultimas modificaciones    
    this.service.registroCambio.subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;      
    });

    //Mostramos la alerta
    this.service.mensajeCambio.subscribe(mensaje => {
      this.alerta.open(mensaje,'Aviso',{duration:3000});
    });

    //this.pacienteService.listar().subscribe(datos => console.log(datos));
    /*this.service.listar().subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;
    });*/

    this.service.listarPageable(0,10).subscribe(data => {
      let registros = JSON.parse(JSON.stringify(data)).content;
      this.cantRegistros = JSON.parse(JSON.stringify(data)).totalElements;
      this.listaDataSource = new MatTableDataSource(registros);
      this.listaDataSource.sort = this.sort;      
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.listaDataSource.filter = filterValue;
  }

  eliminar(id : number){
    this.service.eliminar(id).subscribe(data => {
      this.service.listar().subscribe(data => {
        this.service.registroCambio.next(data);
        this.service.mensajeCambio.next(MSJ_DEL);
      });
    });
  }

  mostrarMas(e : any){
    //console.log(e);
    this.service.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
       let registros = JSON.parse(JSON.stringify(data)).content;
       this.cantRegistros = JSON.parse(JSON.stringify(data)).totalElements;
       this.listaDataSource = new MatTableDataSource(registros);  
       this.listaDataSource.sort = this.sort;     
    });
  }
}
