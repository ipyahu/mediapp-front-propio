import { MSJ_INS, MSJ_MOD } from './../../../shared/var.constants';
import { PacienteService } from './../../../_service/paciente.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Paciente } from '../../../_model/paciente';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  id : number;
  form : FormGroup; //Este sera un objeto que nos servira para hacer binding con nuestro form del html
  edicion : boolean = false; //Esta variable nos va a indicar en que modo se comportará nuestro component EDICION/NUEVO
  registro : Paciente;

  //aqui inyectamos un objeto de tipo route, tambien el service que vamos a usar
  //tambien inyectamos un objeto tipo router, para poder hacer navegaciones desde este componente
  constructor(private route : ActivatedRoute,private router : Router, private service : PacienteService) { //la clase activate route permite gestionar las url's
    //aqui inicializamos los valores para los controles del form en el html
    this.form = new FormGroup({
      'id': new FormControl(0), //Inicializamos el valor para el id en 0
      'nombres' : new FormControl(''),
      'apellidos': new FormControl(''),
      'ci': new FormControl(''),
      'direccion' : new FormControl(''),
      'telefono': new FormControl('')
    });
  } 

  ngOnInit() {
    this.registro = new Paciente();
    /*
      el atributo params del objeto route devuelve los parametros que vienen en la url
      por ello usamos una variable "parametros" para acceder a esos parametros por buena practica
    */
    this.route.params.subscribe( (parametros : Params) => { //la expresion (parametros : Params) => {} --- permite programar una funcion con nuestra logica
      this.id = parametros['id'];
      this.edicion = parametros['id'] != null; //Definimos el valor que tendra la variable "edicion" segun el valor del parametro "id" que viene en la url
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion){
      //cargamos los campos del form
      this.service.listarPacientePorId(this.id).subscribe(datos => { //la expresion datos => {} --- permite programar una funcion con nuestra logica
        //mapeamos el obj que nos devuelve el service al html
        this.form = new FormGroup({
          'id': new FormControl(datos.idPaciente),
          'nombres' : new FormControl(datos.nombres),
          'apellidos': new FormControl(datos.apellidos),
          'ci': new FormControl(datos.ci),
          'direccion' : new FormControl(datos.direccion),
          'telefono': new FormControl(datos.telefono)
        });
      });
    }
  }


  operar(){
    this.registro.idPaciente = this.form.value['id'];
    this.registro.nombres = this.form.value['nombres'];
    this.registro.apellidos = this.form.value['apellidos'];
    this.registro.ci = this.form.value['ci'];
    this.registro.direccion = this.form.value['direccion'];
    this.registro.telefono = this.form.value['telefono'];

    if (this.id){
      //modificar
      this.service.modificar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_MOD);
        });
      }, error => {
        console.log('Ha ocurrido un error en la modificacion del registro!');
      });
    }
    else{
      //insertar
      this.service.registrar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_INS);
        });
      }, error => {
        console.log('Ha ocurrido un error en la insercion del registro!'); //Esta seria una forma de programar en base a errores que pudieran haber en la peticion http, mas adelante veremos como generalizarlos
      });      
    }

    //aqui navegamos a la ruta pacientes que esta dentro del app-rounting.module
    this.router.navigate(['pacientes']);
  }

}
