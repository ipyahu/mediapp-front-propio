import { MSJ_MOD, MSJ_INS } from './../../../shared/var.constants';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ExamenService } from './../../../_service/examen.service';
import { Examen } from './../../../_model/examen';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { formControlBinding } from '@angular/forms/src/directives/reactive_directives/form_control_directive';

@Component({
  selector: 'app-examen-edicion',
  templateUrl: './examen-edicion.component.html',
  styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {

  id : number;
  form : FormGroup; //Este sera un objeto que nos servira para hacer binding con nuestro form del html
  edicion : boolean = false; //Esta variable nos va a indicar en que modo se comportará nuestro component EDICION/NUEVO
  registro : Examen;

  constructor(private service : ExamenService, private router : Router, private route : ActivatedRoute) {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre' : new FormControl(''),
      'descripcion' : new FormControl('')
    });
   }

  ngOnInit() {
    this.registro = new Examen();

    this.route.params.subscribe( (parametros : Params) => { 
      this.id = parametros['id'];
      this.edicion = parametros['id'] != null; 
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion){
      this.service.listarExamenPorId(this.id).subscribe(datos => { 
        this.form = new FormGroup({
          'id': new FormControl(datos.idExamen),
          'nombre' : new FormControl(datos.nombre),
          'descripcion' : new FormControl(datos.descripcion)
        });
      });
    }
  }


  operar(){
    this.registro.idExamen = this.form.value['id'];
    this.registro.nombre = this.form.value['nombre'];
    this.registro.descripcion = this.form.value['descripcion'];

    if (this.id){
      //modificar
      this.service.modificar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_MOD);
        });
      });
    }
    else{
      //insertar
      this.service.registrar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_INS);
        });
      });      
    }

    //aqui navegamos a la ruta pacientes que esta dentro del app-rounting.module
    this.router.navigate(['examenes']);
  }


}
