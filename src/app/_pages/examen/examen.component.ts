import { ActivatedRoute } from '@angular/router';
import { ExamenService } from './../../_service/examen.service';
import { MSJ_DEL } from './../../shared/var.constants';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Examen } from './../../_model/examen';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {

  listaDataSource: MatTableDataSource<Examen>;
  columnasMostradas = ['idExamen','nombre','descripcion','acciones'];
  @ViewChild(MatPaginator) paginator : MatPaginator;
  @ViewChild(MatSort) sort :MatSort; 

  constructor(private service : ExamenService, private alerta :  MatSnackBar, public route : ActivatedRoute) { }

  ngOnInit() {  
    this.service.registroCambio.subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;      
    });

    this.service.mensajeCambio.subscribe(mensaje => {
      this.alerta.open(mensaje,'Aviso',{duration:3000});
    });

    this.service.listar().subscribe(datos => {
      this.listaDataSource = new MatTableDataSource(datos);
      this.listaDataSource.paginator = this.paginator;
      this.listaDataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.listaDataSource.filter = filterValue;
  }

  eliminar(id : number){
    this.service.eliminar(id).subscribe(data => {
      this.service.listar().subscribe(data => {
        this.service.registroCambio.next(data);
        this.service.mensajeCambio.next(MSJ_DEL);
      });
    });
  }
}
