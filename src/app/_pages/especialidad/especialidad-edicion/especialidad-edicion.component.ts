import { Router, Params, ActivatedRoute } from '@angular/router';
import { MSJ_MOD, MSJ_INS } from './../../../shared/var.constants';
import { EspecialidadService } from './../../../_service/especialidad.service';
import { Especialidad } from './../../../_model/especialidad';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-especialidad-edicion',
  templateUrl: './especialidad-edicion.component.html',
  styleUrls: ['./especialidad-edicion.component.css']
})
export class EspecialidadEdicionComponent implements OnInit {

  id : number;
  form : FormGroup; //Este sera un objeto que nos servira para hacer binding con nuestro form del html
  edicion : boolean = false; //Esta variable nos va a indicar en que modo se comportará nuestro component EDICION/NUEVO
  registro : Especialidad;

  constructor(private service : EspecialidadService, private router : Router, private route : ActivatedRoute) {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre' : new FormControl('')
    });
   }

  ngOnInit() {
    this.registro = new Especialidad();

    this.route.params.subscribe( (parametros : Params) => { 
      this.id = parametros['id'];
      this.edicion = parametros['id'] != null; 
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion){
      this.service.listarEspecialidadPorId(this.id).subscribe(datos => { 
        this.form = new FormGroup({
          'id': new FormControl(datos.idEspecialidad),
          'nombre' : new FormControl(datos.nombre)
        });
      });
    }
  }


  operar(){
    this.registro.idEspecialidad = this.form.value['id'];
    this.registro.nombre = this.form.value['nombre'];

    if (this.id){
      //modificar
      this.service.modificar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_MOD);
        });
      });
    }
    else{
      //insertar
      this.service.registrar(this.registro).subscribe(datos => {
        this.service.listar().subscribe(registros => {
          this.service.registroCambio.next(registros);
          this.service.mensajeCambio.next(MSJ_INS);
        });
      });      
    }

    //aqui navegamos a la ruta pacientes que esta dentro del app-rounting.module
    this.router.navigate(['especialidades']);
  }


}
