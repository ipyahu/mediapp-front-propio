export class Paciente{ //la palabra reservada export le agregamos para que la clase pueda ser utilizada en otros typescript
    idPaciente:string;
    nombres:string;
    apellidos:string;
    ci:string;
    direccion:string;
    telefono:string;
}