import { HttpClient } from '@angular/common/http';
import { HOST } from './../shared/var.constants';
import { Subject } from 'rxjs';
import { Examen } from './../_model/Examen';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {

  registroCambio = new Subject<Examen[]>(); //esta es una variable reactiva que almacenará un array de examenes
  mensajeCambio = new Subject<string>();

  url : string = HOST + '/examenes'; //no es una buena practica concatenar asi, pero solo porque me es mas agil por mi teclado :'(

  constructor(private http : HttpClient) { } //creamos e inyectamos en el constructor nuestro objeto capaz de hacer peticiones http

  listar(){
    return this.http.get<Examen[]>(this.url);
  }

  listarExamenPorId(id : number){
    return this.http.get<Examen>(this.url + '/' + id); 
  }

  registrar(obj : Examen){
    return this.http.post<Examen>(this.url,obj);
  }

  modificar(obj : Examen){
    return this.http.post<Examen>(this.url,obj);
  }  

  eliminar(id : number){
    return this.http.delete(this.url + '/' + id);
  }
}
