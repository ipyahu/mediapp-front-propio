import { HttpClient } from '@angular/common/http';
import { HOST } from './../shared/var.constants';
import { Subject } from 'rxjs';
import { Especialidad } from './../_model/Especialidad';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {

  registroCambio = new Subject<Especialidad[]>(); //esta es una variable reactiva que almacenará un array de Especialidades
  mensajeCambio = new Subject<string>();

  url : string = HOST + '/especialidades'; //no es una buena practica concatenar asi, pero solo porque me es mas agil por mi teclado :'(

  constructor(private http : HttpClient) { } //creamos e inyectamos en el constructor nuestro objeto capaz de hacer peticiones http

  listar(){
    return this.http.get<Especialidad[]>(this.url);
  }

  listarEspecialidadPorId(id : number){
    return this.http.get<Especialidad>(this.url + '/' + id); 
  }

  registrar(obj : Especialidad){
    return this.http.post<Especialidad>(this.url,obj);
  }

  modificar(obj : Especialidad){
    return this.http.post<Especialidad>(this.url,obj);
  }  

  eliminar(id : number){
    return this.http.delete(this.url + '/' + id);
  }
}

