import { HttpClient } from '@angular/common/http';
import { HOST } from './../shared/var.constants';
import { Subject } from 'rxjs';
import { Medico } from './../_model/medico';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  registroCambio = new Subject<Medico[]>(); //esta es una variable reactiva que almacenará un array de Medicos
  mensajeCambio = new Subject<string>();

  url : string = HOST + '/medicos'; //no es una buena practica concatenar asi, pero solo porque me es mas agil por mi teclado :'(

  constructor(private http : HttpClient) { } //creamos e inyectamos en el constructor nuestro objeto capaz de hacer peticiones http

  listar(){
    return this.http.get<Medico[]>(this.url);
  }

  listarMedicoPorId(id : number){
    return this.http.get<Medico>(this.url + '/' + id); 
  }

  registrar(obj : Medico){
    return this.http.post<Medico>(this.url,obj);
  }

  modificar(obj : Medico){
    return this.http.post<Medico>(this.url,obj);
  }  

  eliminar(id : number){
    return this.http.delete(this.url + '/' + id);
  }
}
