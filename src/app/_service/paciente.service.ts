import { HOST } from './../shared/var.constants';
import { Paciente } from './../_model/paciente';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  registroCambio = new Subject<Paciente[]>(); //esta es una variable reactiva que almacenará un array de pacientes
  mensajeCambio = new Subject<string>();

  url : string = HOST + '/pacientes'; //no es una buena practica concatenar asi, pero solo porque me es mas agil por mi teclado :'(

  constructor(private http : HttpClient) { } //creamos e inyectamos en el constructor nuestro objeto capaz de hacer peticiones http

  listar(){
    return this.http.get<Paciente[]>(this.url);
  }

  listarPageable(p : number, s: number){
    return this.http.get<Paciente[]>(this.url + '/paginado?page=' + p + '&size=' + s);
  }

  listarPacientePorId(id : number){
    return this.http.get<Paciente>(this.url + '/' + id); 
  }

  registrar(obj : Paciente){
    return this.http.post<Paciente>(this.url,obj);
  }

  modificar(obj : Paciente){
    return this.http.post<Paciente>(this.url,obj);
  }  

  eliminar(id : number){
    return this.http.delete(this.url + '/' + id);
  }
}
